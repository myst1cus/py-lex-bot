# bot.py
import os

import discord
import requests
import string
import re
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
REGISTRATION_KEYWORDS = ["regis"]

LEX_ENDPOINT = "https://kpttedtd2a.execute-api.us-east-1.amazonaws.com/lex?userid={0}"


if not TOKEN:
    TOKEN = os.environ.get('DISCORD_TOKEN')

if not TOKEN:
    TOKEN = 'NzYxNjkwNTAxMjU0MDg2Njg2.X3eRjw.q4xawKBwwSjwoefhqrK9p3a9FYs'


client = discord.Client()

welcome_message = "Hello {0}! Welcome to the channel!"

@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!')
    for guild in client.guilds:
        for channel in guild.channels:
            print (f"I am connected to the {channel} channel in the {guild} guild.")

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    
    print("Found message in following message channel : {0}".format(message.channel))

    # here is where we'd want the logic to summon the bot to DM a user.
    # this may be a reaction in a main channel, or a channel that has instructions on what to expect.
    if 'Direct Message' not in str(message.channel):
        for keyword in REGISTRATION_KEYWORDS:
            if keyword in message.content:
                await message.author.send(welcome_message.format(message.author.name))
                return

    if 'Direct Message' in str(message.channel):
        request_data = str(message.content)
        print("Following user ID messaged me : {0}".format(message.author.id))
        response = requests.post(LEX_ENDPOINT.format(message.author.id), data=request_data)
        print((response.content.decode('utf-8')))
        await message.channel.send(str(response.content.decode('utf-8')))
        return
    return
    
@client.event
async def on_member_join(member):
    await member.send(welcome_message.format(member.name))
    print("Sent registration message to : " + member.name)


client.run(TOKEN)

